const widthInput = document.querySelector("#boardWidth");

document.querySelector("#solveBtn").addEventListener("click", () => {
  if (widthInput.value >= 27) {
    alert("maximum board size is 26");
    return;
  }
  if (
    widthInput.value >= 11 &&
    !confirm("computation above 11 will some time do you want to continiue?")
  ) {
    return;
  }
  $(".chessBoardContainer").html("");
  $("#solutionCount").text("Loading");

  setTimeout(() => {
    fillBoardByQueens(widthInput.value);
  }, 100);
});

const squaresArray = n => {
  const table = [];
  for (let i = n; i > 0; i--) {
    table.push(new Array(n).fill(0));
  }
  return table;
};

const createHtmlTable = width => {
  const board = squaresArray(width);
  let boardHtml = `<div class="chessBoard">${board
    .map(row => {
      return `<div class="row">
                ${board.map(square => `<div></div>`).join("")}
            </div>`;
    })
    .join("")}</div>`;
  $(".chessBoardContainer").append(boardHtml);
};

const fillBoardByQueens = width => {
  var queenAlgorithm = new Queen(width);
  queenAlgorithm.run();
  if (queenAlgorithm.solutions.length === 0) {
    createHtmlTable(width);
    $("#solutionCount").text(`solutions: ${queenAlgorithm.solutions.length}`);
    return;
  }

  const queenStructredSolutions = [];
  for (var indexA = 0; indexA < queenAlgorithm.solutions.length; ++indexA) {
    var solution = queenAlgorithm.solutions[indexA];
    let s = [];
    for (var indexB = 0; indexB < solution.length; ++indexB) {
      s.push([indexB + 1, solution[indexB] + 1]);
    }
    queenStructredSolutions.push(s);
  }
  $("#solutionCount").text(`solutions: ${queenStructredSolutions.length}`);
  for (let j = 0; j < queenStructredSolutions.length; j++) {
    createHtmlTable(width);
    let randomSolution = queenStructredSolutions[j];
    for (let i = 0; i < randomSolution.length; i++) {
      $(".chessBoard")
        .eq(j)
        .find(`.row:eq(${randomSolution[i][0] - 1})`)
        .find(`div:eq(${randomSolution[i][1] - 1})`)
        .text("Queen");
    }
  }
};

const randNumber = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

///////// Algorithm:  http://code.activestate.com/recipes/578497-eight-queen-problem-javascript/
var OCCUPIED = 1; // field is in use
var FREE = 0; // field is not in use

function Queen(width) {
  this.width = width;
  this.lastRow = this.width - 1;
  this.columns = new Array(this.width);
  this.rcolumns = new Array(this.width);

  var numberOfDiagonals = 2 * this.width - 1;
  this.diagonals1 = new Array(numberOfDiagonals);
  this.diagonals2 = new Array(numberOfDiagonals);
  this.solutions = new Array();

  for (var index = 0; index < numberOfDiagonals; ++index) {
    if (index < this.width) {
      this.columns[index] = -1;
    }
    this.diagonals1[index] = FREE;
    this.diagonals2[index] = FREE;
  }

  // starts the search with initial parameters
  this.run = function() {
    this.calculate(0);
    return this;
  };

  // searches for all possible solutions
  this.calculate = function(row) {
    for (var column = 0; column < this.width; ++column) {
      // current column blocked?
      if (this.columns[column] >= 0) {
        continue;
      }

      // relating diagonale '\' depending on current row and column
      var ixDiag1 = row + column;
      if (this.diagonals1[ixDiag1] == OCCUPIED) {
        continue;
      }

      // relating diagonale '/' depending on current row and column
      var ixDiag2 = this.width - 1 - row + column;
      if (this.diagonals2[ixDiag2] == OCCUPIED) {
        continue;
      }

      // occupying column and diagonals depending on current row and column
      this.columns[column] = row;
      this.diagonals1[ixDiag1] = OCCUPIED;
      this.diagonals2[ixDiag2] = OCCUPIED;

      if (row == this.lastRow) {
        this.solutions.push(this.columns.slice());
      } else {
        this.calculate(row + 1);
      }

      this.columns[column] = -1;
      this.diagonals1[ixDiag1] = FREE;
      this.diagonals2[ixDiag2] = FREE;
    }
  };
}
